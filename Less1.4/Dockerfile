#building nginx from sources 
FROM debian:9 AS builder

RUN apt-get -y update &&  apt install -y \
   wget \
   gcc \
   libpcre3-dev \
   zlib1g-dev \
   build-essential 

RUN wget http://luajit.org/download/LuaJIT-2.0.5.tar.gz \
    && tar xvfz LuaJIT-2.0.5.tar.gz && mv LuaJIT-2.0.5 LuaJIT-2.0 \
    && cd LuaJIT-2.0 \
    && make && make install PREFIX=/usr/local/luajit/

ENV LUAJIT_LIB=/usr/local/luajit/lib
ENV LUAJIT_INC=/usr/local/luajit/include/luajit-2.0

RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz && tar -zxvf v0.3.1.tar.gz -C /usr/lib
RUN wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.19.tar.gz && tar -zxvf v0.10.19.tar.gz -C /usr/lib
RUN wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.21.tar.gz && tar -zxvf v0.1.21.tar.gz -C /usr/lib
RUN wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.11.tar.gz && tar -zxvf v0.11.tar.gz -C /usr/lib


RUN wget https://openresty.org/download/nginx-1.19.3.tar.gz \
   &&  tar -xzvf nginx-1.19.3.tar.gz \
   &&  cd nginx-1.19.3/ \
        && ./configure --prefix=/usr/local/nginx \
         --with-ld-opt="-Wl,-rpath,/usr/local/luajit/lib" \
         --add-module=/usr/lib/ngx_devel_kit-0.3.1 \
         --add-module=/usr/lib/lua-nginx-module-0.10.19 &&  make -j2 && make install \
    && cd /usr/lib/lua-resty-core-0.1.21 && make install PREFIX=/usr/local/nginx \
    && cd /usr/lib/lua-resty-lrucache-0.11 && make install PREFIX=/usr/local/nginx

#run nginx
FROM debian:9 

#run binary
COPY --from=builder /usr/local/nginx/sbin/nginx /usr/local/nginx/sbin/nginx
COPY --from=builder /usr/local/luajit/ /usr/local/luajit/
COPY --from=builder /usr/local/nginx/logs  /usr/local/nginx/logs
COPY --from=builder /usr/local/nginx/conf /usr/local/nginx/conf
COPY --from=builder /usr/local/nginx/html /usr/local/nginx/html
COPY --from=builder /usr/local/nginx/lib/lua /usr/local/nginx/lib/lua
RUN  chmod +x /usr/local/nginx/sbin/nginx
ENV PATH="${PATH}:/usr/local/nginx/sbin"

CMD ["nginx", "-g", "daemon off;"]



